package com.example.studentmsclient.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class HttpClientConfig {

    @Bean("webClientStudent")
    //@Qualifier("webClientStudent")
    //@Primary
    @LoadBalanced
    public WebClient.Builder webClientBuilderStudent() {
        return WebClient.builder().baseUrl("http://studentms");
    }

    @Bean("webClientStudent1")
    public WebClient webClientStudent(@Qualifier("webClientStudent") WebClient.Builder studentBuilder) {
        return studentBuilder.build();
    }
}
