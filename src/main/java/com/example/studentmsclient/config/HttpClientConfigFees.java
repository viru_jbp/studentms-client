package com.example.studentmsclient.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class HttpClientConfigFees {

    @Bean("webClientFees")
    //@Qualifier("webClientFees")
    //@Primary
    @LoadBalanced
    public WebClient.Builder webClientBuilderFees() {
        return WebClient.builder().baseUrl("http://feesms");
    }

    @Bean("webClientFees1")
    public WebClient webClientFees(@Qualifier("webClientFees") WebClient.Builder feesBuilder) {
        return feesBuilder.build();
    }
}
