package com.example.studentmsclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentmsClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentmsClientApplication.class, args);
	}

}
