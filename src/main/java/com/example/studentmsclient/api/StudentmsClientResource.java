package com.example.studentmsclient.api;

import com.example.studentmsclient.model.Fees;
import com.example.studentmsclient.model.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreakerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.URISyntaxException;
import java.util.List;

@RestController
public class StudentmsClientResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentmsClientResource.class);
    @Autowired(required=true)
    @Qualifier("webClientStudent1")
    private WebClient webClientStudent;

    @Autowired(required=true)
    @Qualifier("webClientFees1")
    private WebClient webClientFees;
    @Autowired
    private ReactiveCircuitBreakerFactory circuitBreakerFactory;

    private WebClient feesWebClient;

    @GetMapping("/students-client")
    public Mono<ResponseEntity<List<Student>>> getAllUsersFromStudentms() {
//        List<User>.class;
        LOGGER.info("studentms-client is called from userms");
        Mono<ResponseEntity<List<Student>>> responseEntityMono = webClientStudent.get().uri("/students").retrieve().toEntity(
                new ParameterizedTypeReference<List<Student>>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    Student student = new Student(); // or use a cache or call backupms
                    student.setName("Fallback student");
                    return Mono.just(ResponseEntity.ok((List.of(student))));
                }));

        return responseEntityMono;
    }

    @GetMapping("/student-client/{id}")
    public Mono<ResponseEntity<Student>> getStudentFromStudentms(@PathVariable Integer id) {
//        List<User>.class;
        LOGGER.info("studentms-client is called from studentms, get single student info " + id);
        Mono<ResponseEntity<Student>> responseEntityMono = webClientStudent.get().uri("/student/" + id).retrieve().toEntity(
                new ParameterizedTypeReference<Student>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    Student student = new Student(); // or use a cache or call backupms
                    student.setName("Fallback student");
                    return Mono.just(ResponseEntity.ok((student)));
                }));
        return responseEntityMono;
    }


    @PostMapping("/students-client")
    public Mono<ResponseEntity<Student>> addStudent(@RequestBody Student student) throws URISyntaxException {
        LOGGER.info("Saving Student-client from studentms");

        Mono<ResponseEntity<Student>> responseEntityMono = webClientStudent.post().uri("/students").bodyValue(student).retrieve().toEntity(
                new ParameterizedTypeReference<Student>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    //Student student = new Student(); // or use a cache or call backupms
                    student.setName("Fallback student");
                    return Mono.just(ResponseEntity.ok((student)));
                }));
        return responseEntityMono;
    }


    @DeleteMapping("/deleteStudent-client/{id}")
    public Mono<ResponseEntity<String>> deleteStudentFromStudentms(@PathVariable Integer id) {
        LOGGER.info("deleteStudent-client is called from studentms, to delete the student info " + id);
        Mono<ResponseEntity<String>> responseEntityMono = webClientStudent.delete().uri("/deleteStudent/" + id).retrieve().toEntity(
                new ParameterizedTypeReference<String>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    return Mono.just(ResponseEntity.ok(("Not Found " + id)));
                }));
        return responseEntityMono;
    }

    @PutMapping("/updateStudent-client/{id}")
    public Mono<ResponseEntity<Student>> updateStudent(@PathVariable Integer id, @RequestBody Student studentDetails) {
        LOGGER.info("Saving Student-client from studentms");

        Mono<ResponseEntity<Student>> responseEntityMono = webClientStudent.put().uri("/updateStudent/" + id).bodyValue(studentDetails).retrieve().toEntity(
                new ParameterizedTypeReference<Student>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    //Student student = new Student(); // or use a cache or call backupms
                    studentDetails.setName("Fallback student");
                    return Mono.just(ResponseEntity.ok((studentDetails)));
                }));
        return responseEntityMono;
    }


    @GetMapping("/hello-client")
    public String getHelloFromClient() {
        return "Hello Client";

    }

    @GetMapping("/students-client-fees")
    public Mono<ResponseEntity<List<Fees>>> getAllStudentFeesFromFeesms() {
        LOGGER.info("studentms-client-fees is called from feesms");
        Mono<ResponseEntity<List<Fees>>> responseEntityMono = webClientFees.get().uri("/fees").retrieve().toEntity(
                new ParameterizedTypeReference<List<Fees>>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    Fees fees = new Fees(); // or use a cache or call backupms
                    fees.setStudentFees(0);
                    return Mono.just(ResponseEntity.ok((List.of(fees))));
                }));

        return responseEntityMono;
    }

    @PostMapping("/students-client-fees")
    public Mono<ResponseEntity<Fees>> addStudentFees(@RequestBody Fees fees) throws URISyntaxException {
        LOGGER.info("Saving Student-client from studentms");

        Mono<ResponseEntity<Fees>> responseEntityMono = webClientFees.post().uri("/fees").bodyValue(fees).retrieve().toEntity(
                new ParameterizedTypeReference<Fees>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    //Student student = new Student(); // or use a cache or call backupms
                    fees.setStudentFees(0);
                    return Mono.just(ResponseEntity.ok((fees)));
                }));
        return responseEntityMono;
    }


}
