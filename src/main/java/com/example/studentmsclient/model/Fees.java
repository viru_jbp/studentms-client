package com.example.studentmsclient.model;


public class Fees {
    private Integer id;
    private Integer studentId;
    private Integer studentFees;

    public Integer getId() {
        return id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public Integer getStudentFees() {
        return studentFees;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public void setStudentFees(Integer studentFees) {
        this.studentFees = studentFees;
    }
}
